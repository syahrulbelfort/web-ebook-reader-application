const authController = require('../controller/authController');
const express = require('express');
const router = express.Router();
const fileController = require('../controller/files');

router.use('/file', express.static('uploads/pdf'));
router.get('/api/verify', authController.verify );
router.post('/api/login', authController.login);
router.post('/uploads', fileController.uploads);
router.get('/download/:filename', fileController.download);


module.exports = router;
