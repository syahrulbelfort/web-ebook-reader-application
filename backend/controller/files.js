const multer = require('multer');
const File = require('../models/file'); // Import the File model


// configure storage for uploaded files
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './uploads/pdf');
    },
    filename: function (req, file, cb) {
      const ext = file.originalname.split('.').pop();
      cb(null, Date.now() + '.' + ext);
    }
  });

  // create an instance of the multer middleware with the storage configuration
const upload = multer({ storage });

  async function uploads(req, res) {
    try {
      upload.single('file')(req, res, async (err) => {
        if (err) {
          console.error(err);
          res.status(500).send('An error occurred while uploading the file.');
        } else {
          const file = req.file;
          const user_id = req.body.user_id;
        
          // create a new file object with the metadata
          const newFile = new File({
            user_id: user_id,
            filename: file.filename,
            originalname: file.originalname,
            mimetype: file.mimetype,
            size: file.size,
            url :`http://localhost:3000/file/${req.file.filename}`
          });
        
          // save the new file object to the database
          const savedFile = await newFile.save();
        
          // return the saved file object to the client
          res.status(200).json(savedFile);
        }
      });
    } catch (error) {
      console.error(error);
      res.status(500).send('An error occurred while uploading the file.');
    }
  }
  
  
  

function download(req, res) {
    try {
      const filename = req.params.filename;
      const path = `./uploads/pdf/${filename}`;
  
      // send the file to the client
      res.download(path, filename, (err) => {
        if (err) {
          console.error(err);
          res.status(500).send('An error occurred while downloading the file.');
        }
      });
    } catch (error) {
      console.error(error);
      res.status(500).send('An error occurred while downloading the file.');
    }
  }

  module.exports = {
   download,
   uploads
  };
