const jwt = require('jsonwebtoken');
const User = require('../models/users');

function verify(req, res) {
  const token = req.headers.authorization?.split(' ')[1];

  if (!token) {
    return res.status(401).json({ message: 'Authorization header not found.' });
  }

  try {
    const decoded = jwt.verify(token, 'your-secret-key');
    res.status(200).json(decoded);
  } catch (error) {
    res.status(401).json({ message: 'Invalid token.' });
  }
}

async function login(req, res) {
  const { username, password } = req.body;

  try {
    const user = await User.findOne({ username, password });

    if (!user) {
      return res.status(401).json({ message: 'Invalid credentials' });
    }

    const { id, full_name, role } = user;

    // Create a JWT token
    const token = jwt.sign({ id, username, role }, 'your-secret-key');
    return res.json({ full_name, role, id, username, token });
  } catch (err) {
    console.error(err);
    return res.status(500).json({ message: 'Internal server error' });
  }
}

module.exports = {
  login,
  verify,
};
