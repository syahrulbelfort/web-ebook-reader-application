const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;
const loginRoute = require('./routes/router') 
const User = require('./models/users');
const seed = require('./models/seeds/userSeeders');
var cors = require("cors");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use('/', loginRoute)

async function connectToMongoDB(){
    try {
       await mongoose.connect('mongodb://localhost:27017/myapp', { useNewUrlParser: true })
       console.log(`Connected to MongoDB`)
    } catch (error) {
        console.error('MongoDB connection failed:', error.message)
    }
}

connectToMongoDB()




seed(User);

app.listen(port, () => console.log(`Server running on port ${port}`));
