const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  username: String,
  password: String,
  full_name: String,
  role: String,
  files:Array
});

const User = mongoose.model('User', userSchema);

module.exports = User;
