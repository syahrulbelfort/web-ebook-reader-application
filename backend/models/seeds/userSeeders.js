const jwt = require('jsonwebtoken');
const User = require('../users');

async function seed() {
  const users = [
    {
      username: 'syahrul',
      password: 'password123',
      full_name: 'Syahrul Kurniawan',
      role: 'student',
      files: []
    },
    {
      username: 'sella',
      password: 'password456',
      full_name: 'Sella Efrida',
      role: 'student',
      files: []
    }
  ];

  try {
    await User.deleteMany();
    for (const user of users) {
      const newUser = new User(user);
      const token = jwt.sign({ id: newUser._id, username: newUser.username, role: newUser.role, files: newUser.files }, 'your-secret-key');
      newUser.token = token;
      await newUser.save();
    }
    console.log('Users seeded successfully');
  } catch (err) {
    console.error(err);
  }
}

module.exports = seed;

