import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './App.css'
import {UserProvider} from './context/useContext'
import 'bootstrap/dist/css/bootstrap.min.css';


const rootElement = document.getElementById('root');
ReactDOM.createRoot(rootElement).render(
  <UserProvider>
  <App />
  </UserProvider>
);
