import React, {createContext, useState} from 'react'

export const UserContext = createContext()

export const UserProvider = ({children}) => {
    const [user, setUser]= useState(null)
    const [showPdf, setShowpdf] = useState(false)

  return (
        <UserContext.Provider value={{user,setUser, showPdf, setShowpdf}}>
            {children}
        </UserContext.Provider>
    )
}
