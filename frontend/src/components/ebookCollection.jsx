import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { UserContext } from '../context/useContext';
import PdfViewer from './samples/pdfViewer'
import { Button,Table } from 'react-bootstrap';

function EbookCollection() {
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploadedFiles, setUploadedFiles] = useState([]);
  const [link, setLink] = useState('')
  const [filename, setFilename] = useState('')
  const { user, showPdf, setShowpdf } = useContext(UserContext)


  useEffect(() => {
  const files = JSON.parse(window.localStorage.getItem('files'));
  if (files) {
    setUploadedFiles(files);
  }
}, []);
  

  useEffect(()=>{
    window.localStorage.setItem('files', JSON.stringify(uploadedFiles))
  }, [uploadedFiles])

 
  const handleFileChange = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  const handleFileUpload = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append('file', selectedFile);
    formData.append('user_id', user.id);
    try {
      const response = await axios.post('http://localhost:3000/uploads', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
      });
      setUploadedFiles([...uploadedFiles, response.data]);
      setSelectedFile(null);
    } catch (error) {
      console.error(error);
    }
  };
  
  const userUploadedFiles = uploadedFiles.filter((file) => file.user_id === user.id);


  const handleFileDownload = async (filename) => {
    try {
      const response = await axios.get(`http://localhost:3000/download/${filename}`, {
        responseType: 'blob',
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', filename);
      document.body.appendChild(link);
      link.click();
    } catch (error) {
      console.error(error);
    }
  };
 
  function handleFileView(filename){
    setLink(`http://localhost:3000/file/${filename}`)
    setFilename(`${filename}`)
    setShowpdf(true)
  }

  function handleClose(){
    setShowpdf(false)
  }
  


  return (
    <div className=''>
      <h2>Ebook Collection</h2>
      <form onSubmit={handleFileUpload}>
        <label>
          Upload file:
          <input name='file' type="file" onChange={handleFileChange} />
        </label>
        <Button type="submit">Upload</Button>
      </form>
        <Table striped bordered hover>
          <thead>
            <tr>
            <h3 className='mt-5'>Your Uploaded Files</h3>
            </tr>
            <tbody>
            {userUploadedFiles.length > 0 && (
        <>
          <ul>
            {userUploadedFiles.map((file,id) => (
              <li className='mb-3 li-margin' key={id}>
                {file.filename}
                <Button className='ms-5 btn-margin' onClick={() => handleFileDownload(file.filename)}>Download</Button>
               {showPdf ? null : <Button onClick={() => handleFileView(file.filename)}>Open E-book</Button> } 
              </li>
            ))}
          </ul>
        </>
      )}
            </tbody>
          </thead>
        </Table>
 
      { showPdf ? <Button variant='danger' onClick={handleClose}>Close E-book</Button> : null}
      {showPdf && <PdfViewer url={link} fileName={filename} />}
     
    </div>
  );
}

export default EbookCollection;
