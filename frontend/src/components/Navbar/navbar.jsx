import React, {useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import { UserContext } from '../../context/useContext';

function NavBar() {

    const { user } = useContext(UserContext)

    function handleClick(){
        localStorage.clear();
        window.location.href='/'
      }
    
  return (
    <>
      <Navbar className='nav-background' variant="dark">
        <Container>
          <Navbar.Brand href="#home">
            <img
              alt=""
              src="/E-Book.png"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{' '}
            E-Book Reader
          </Navbar.Brand>
            {user ? <a href='/' onClick={handleClick}>Logout</a> : null}
        </Container>
      </Navbar>
    </>
  );
}

export default NavBar;