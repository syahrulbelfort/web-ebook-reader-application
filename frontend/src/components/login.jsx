import React, { useContext, useEffect, useState } from 'react';
import axios from 'axios';
import {Form,Button} from 'react-bootstrap'
import { useNavigate } from 'react-router-dom';
import { UserContext } from '../context/useContext'
import EbookCollection from './ebookCollection'

function Login() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState(''); 
    const {user, setUser} = useContext(UserContext)
    const navigate = useNavigate();

    const handleSubmit = async (e) =>{
        try {
            
            const user = {username, password}
            e.preventDefault()
            const response = await axios.post(
                'http://localhost:3000/api/login', user
            )
            setUser(response.data)
            localStorage.setItem('user', JSON.stringify(response.data))
            localStorage.setItem('token', response.data.token)
        } catch (error) {
            if(error){
                alert(`password salah!`)
                navigate('/')
            }
        }
    }

    useEffect(() => {
        const login = localStorage.getItem('user');
        if (login) {
            const foundUser = JSON.parse(login)
          setUser(foundUser);
        }
      }, [setUser, navigate]);
    
     

      
    
      if(user){
        return (
            <>
        <div className='margin-top-custom user-layout'>
          <h5>Welcome, {user.full_name}!</h5>
          <h5 className='role-m'>Your role : {user.role}</h5>
          <EbookCollection user={user} />
        </div>
            </>
        )
      }

      

  return (
    <div className='login-container'>
        <h1>Login to read e-Reader</h1>
       <Form onSubmit={handleSubmit}>
      <Form.Group  className="mb-3" controlId="formBasicEmail">
        <Form.Label>Username</Form.Label>
        <Form.Control type="username" placeholder="Enter username" value={username} onChange={(e)=>{
            setUsername(e.target.value)
        }} />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password"  onChange={(e)=>{
            setPassword(e.target.value)
        }} />
      </Form.Group>
      <Button className='btn-background' type="submit">
        Login
      </Button>
    </Form>
    </div>
  );
}

export default Login;
