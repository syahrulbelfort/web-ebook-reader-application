import React from 'react';
import Login from './components/login';
import NavBar from './components/Navbar/navbar';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <NavBar/>
      <Routes>
        <Route  path='/' element={<Login/>} />
      </Routes>
    </Router>
  );
}

export default App;
